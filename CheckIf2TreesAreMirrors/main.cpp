#include <iostream>
#include <vector>


using namespace std;


typedef struct node_structure
{
    int                   data;
    struct node_structure *left;
    struct node_structure *right;
}node_struct;

class BinaryTree
{
    private:
        vector<int> preorderTraversalVector;
        vector<int> postorderTraversalVector;
    
        node_struct* createNewNode(int data)
        {
            if(data == INT_MIN)
            {
                return NULL;
            }
            node_struct *newNode = new node_struct();
            if (newNode)
            {
                newNode->data  = data;
                newNode->left  = NULL;
                newNode->right = NULL;
            }
            return newNode;
        }
    
        vector<node_struct *> createNodesAndPutItInVector(vector<int> allNodesDataVector)
        {
            vector<node_struct *> allTreeNodesVector;
            int                   allNodesDataVectorSize = (int)(allNodesDataVector.size());
            for (int i = 0 ; i < allNodesDataVectorSize ; i++)
            {
                allTreeNodesVector.push_back(createNewNode(allNodesDataVector[i]));
            }
            return allTreeNodesVector;
        }
    
        void preorderTraversalOfTreeWithHeadNode(node_struct *head)
        {
            if(!head)
            {
                return;
            }
            
            preorderTraversalVector.push_back(head->data);
            preorderTraversalOfTreeWithHeadNode(head->left);
            preorderTraversalOfTreeWithHeadNode(head->right);
        }
    
        void postorderTraversalOfTreeWithHeadNode(node_struct *head)
        {
            if(!head)
            {
                return;
            }
            
            postorderTraversalOfTreeWithHeadNode(head->left);
            postorderTraversalOfTreeWithHeadNode(head->right);
            postorderTraversalVector.push_back(head->data);
        }
    
    public:
        vector<node_struct *> createBinaryTreeFromVector(vector<int> firstTreeNodesDataVector , vector<int> secondTreeNodesDataVector)
        {
            int                   firstTreeDataVectorSize  = (int)(firstTreeNodesDataVector.size());
            int                   secondTreeDataVectorSize = (int)(secondTreeNodesDataVector.size());
            vector<node_struct *> firstTreeAllNodesVector  = createNodesAndPutItInVector(firstTreeNodesDataVector);
            vector<node_struct *> secondTreeAllNodesVector = createNodesAndPutItInVector(secondTreeNodesDataVector);
            node_struct           *firstTreeHead           = firstTreeAllNodesVector[0];
            node_struct           *secondTreeHead          = secondTreeAllNodesVector[0];
            vector<node_struct *> headsVector              = {firstTreeHead , secondTreeHead};
            int                   index                    = 0;
            int                   progressiveIndex         = 1;
            while (index+progressiveIndex < firstTreeDataVectorSize-1)
            {
                firstTreeAllNodesVector[index]->left   = firstTreeAllNodesVector[index+progressiveIndex];
                firstTreeAllNodesVector[index]->right  = firstTreeAllNodesVector[index+progressiveIndex+1];
                index++;
                progressiveIndex                      += 1;
            }
            index            = 0;
            progressiveIndex = 1;
            while (index+progressiveIndex < secondTreeDataVectorSize-1)
            {
                secondTreeAllNodesVector[index]->left  = secondTreeAllNodesVector[index+progressiveIndex];
                secondTreeAllNodesVector[index]->right = secondTreeAllNodesVector[index+progressiveIndex+1];
                index++;
                progressiveIndex                      += 1;
            }
            return headsVector;
        }
    
        bool checkIfTheTreesWithHeadsAreMirrorsOfEachOtherOrNot(node_struct *treeOneHead , node_struct *treeTwoHead)
        {
            preorderTraversalOfTreeWithHeadNode(treeOneHead);
            postorderTraversalOfTreeWithHeadNode(treeTwoHead);
            int preorderTreeVectorSize  = (int)(preorderTraversalVector.size());
            int postorderTreeVectorSize = (int)(postorderTraversalVector.size());
            if (preorderTreeVectorSize != postorderTreeVectorSize)
            {
                return false;
            }
            int start                   = -1;
            int end                     = preorderTreeVectorSize;
            while (start < end)
            {
                if((preorderTraversalVector[++start] != postorderTraversalVector[--end]) || (preorderTraversalVector[end] != postorderTraversalVector[start]))
                {
                    return false;
                }
            }
            if (start == end && preorderTraversalVector[start] != postorderTraversalVector[start])
            {
                return false;
            }
            return true;
        }
};

int main(int argc, const char * argv[])
{
    vector<int>           firstTreeNodesDataVector  = {1 , 2 , 3 , 4 , 5 , 6 , 7 , INT_MIN , INT_MIN , 8 , 9 , INT_MIN , INT_MIN , INT_MIN , 12};//{1 , 2 , 3 , INT_MIN , INT_MIN , 4 , 5};
    vector<int>           secondTreeNodesDataVector = {1 , 3 , 2 , 7 , 6 , 5 , 4 , 12 , INT_MIN , INT_MIN , INT_MIN , 9 , 8 , INT_MIN , INT_MIN};//{1 , 3 , 2 , 5 , 4 , INT_MIN , INT_MIN};
    BinaryTree            bTree                     = BinaryTree();
    vector<node_struct *> headsVector               = bTree.createBinaryTreeFromVector(firstTreeNodesDataVector , secondTreeNodesDataVector);
    bTree.checkIfTheTreesWithHeadsAreMirrorsOfEachOtherOrNot(headsVector[0] , headsVector[1]) ? cout<<"YES"<<endl : cout<<"NO"<<endl;
    return 0;
}
